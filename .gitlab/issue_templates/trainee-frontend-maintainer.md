<!--
  Update the title of this issue to: Trainee FE maintainer (App) - [full name]
  Where App can be:
  - GitLab
  - customers-license
  - version
-->

## Basic setup

1. [ ] Read the [code review page in the handbook](https://about.gitlab.com/handbook/engineering/workflow/code-review/) and the [code review guidelines](https://docs.gitlab.com/ee/development/code_review.html).
2. [ ] Understand [how to become a maintainer](https://about.gitlab.com/handbook/engineering/workflow/code-review/#how-to-become-a-maintainer) and add yourself as a [trainee maintainer](https://about.gitlab.com/handbook/engineering/workflow/code-review/#trainee-maintainer) on the [team page](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/data/team.yml).

## Working towards becoming a maintainer

These are only guidelines. Remember that there is no specific timeline on this.

As part of your journey towards becoming a maintainer, you may find it useful to:

1. [ ] Act as a coach in a big deliverable that requires [following the planning step](https://docs.gitlab.com/ee/development/fe_guide/development_process.html#planning-development) as part of the trainee program.
1. [ ] [Shadow a maintainer](#code-review-pairing) while they review an MR. This will allow you to get insight into the thought processes involved.
1. [ ] [Have a maintainer shadow _you_](#code-review-pairing) while you review an MR _as if you were a maintainer_ . Ideally, this would be with a different maintainer to the above, so you can get different insights.

It is up to you to ensure that you are getting enough MRs to review, and of
varied types. All engineers are reviewers, so you should already be receiving
regular reviews from Reviewer Roulette. You could also seek out more reviews
from your team, or #frontend Slack channels.

Your reviews should aim to cover maintainer responsibilities as well as reviewer
responsibilities. Your approval means you think it is ready to merge.


After each MR is merged or closed, add a discussion to this issue using this
template:

```markdown
### (Merge request title): (Merge request URL)

During review:

- (List anything of note, or a quick summary. "I suggested/identified/noted...")

Post-review:

- (List anything of note, or a quick summary. "I missed..." or "Merged as-is")

(Maintainer who reviewed this merge request) Please add feedback, and compare
this review to the average maintainer review.
```

**Note:** Do not include reviews of security MRs because review feedback might
reveal security issue details.

### Code Review Pairing

Much like pair programming, pairing on code review is a great way to knowledge share and collaborate on merge request. This is a great activity for trainee maintainers to participate with maintainers for learning their process of code review.

Out of consideration for our contributors, please consider gaining consent from the merge request author before:

- Hosting a private review pairing session involving a group of shadowers (i.e. more than 1)
- Hosting a public review pairing session (i.e. published to GitLab Unfiltered)

You **must** gain consent from the merge request author before:

- Hosting a public review pairing session on a Community Contribution

Consent is not needed for:

- If only one person is shadowing a code review from a maintainer

## When you're ready to make it official

When reviews have accumulated, and recent reviews consistently fulfill
maintainer responsibilities, any maintainer can take the next step. The trainee
should also feel free to discuss their progress with their manager or any
maintainer at any time.

1. [ ] Create a merge request for [team page](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/data/team.yml) proposing yourself as a maintainer.
2. [ ] Keep reviewing, start merging :metal:

/label ~"trainee maintainer"
