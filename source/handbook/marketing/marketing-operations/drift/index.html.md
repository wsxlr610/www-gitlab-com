---
layout: handbook-page-toc
title: "Drift"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Using Drift

Drift is a chat platform our Sales Development team uses to engage site visitors. 

## Drift requests & questions 
* **Slack channel:** #drift
* **Support:** Chat support (bottom left-hand corner of the Drift app)
* **MktgOps project issue:** State the reason for the issue and leave any detailed information in regards to the request being made. Use the following labels:
    * `MktgOps`: The starting point for any label that involves MktgOps. The MktgOps team subscribes to this label and will see the request being made.
    * `Drift`: Issue is directly related to the Drift chat tool. Specific admins for this tool are subscribed to this label.
    * `SDR`: If your request is from or involves the SDR team


## Rules of engagement  

-   Set yourself to away when done working / no longer want to receive chats
-   SLA: please respond to chats within 30 seconds
-   If you are routed a chat from outside your territory, assist and qualify by continuing the conversation. If a lead is then created in Salesforce, change it to the appropriate owner.
-   In the event you aren't able to assist a site visitor on your own and escalation is needed, please reach out to your manager and/or the #drift slack channel

## Drift resources & training materials

**Connect calendar and set hours**

*  Click your Avatar > Choose My Settings > Choose My Calendar and get connected.
*  Once connected, you can edit the following:
    * Description of the meeting
    * Duration of the meeting
    * Times you are available/time zone
    * Buffer time/minimum notice time

**Set yourself to available or away**

*  Change your status by clicking your avatar in the bottom left-hand corner. Whether you are available or away, you can still have your notifications on or off.
*  **Note:** If you close out of Drift, your status will not change. The only way to change your status is by updating in the tool.

**Chat notifications**
* There are various ways in which you can be notified of a chat. [This article](https://gethelp.drift.com/hc/en-us/articles/360019501974-How-to-Set-Up-Up-Your-Notifications-Preferences) will walk you through how to set up or change notifications. 

**Close or open a conversations**
*  You can change your conversation status by clicking the drop-down in the top right-hand corner of your conversation.

**Marking a chat as a CQL**
*  A CQL is a Chat Qualified Lead. 
*  On the right hand side of your conversations view, you will be able to see information about the site visitor. Right below their name, you will see the four options of CQL in the form of a lightning bolt. Please mark a lead with the red line for `unqualified lead`, one lightning bolt for `Inquiry`, and three lightning bolts for `MQL`. Refer to this [term glossary](https://about.gitlab.com/handbook/business-ops/resources/#glossary) for a refresher on `Inquiry` and `MQL` definitions. 

**Sharing your calendar in chat**
*  At the right-hand side of the conversation toolbar there is a small calendar icon. Click there and you can share your own calendar or anyone else’s calendar that is connected in Drift.

## Drift routing

Drift is expected to mirror [LeanData](https://about.gitlab.com/handbook/marketing/marketing-operations/leandata/) routing as closely as possible. If there is an exception it will be listed here as more complex routing is rolled out. Our goal is to only connect site visitors with their aligned resources in the SDR organization. This means site visitors during offline hours will be asked to schedule a meeting with their SDR. 


## Drift playbooks
| Type | Name |
| :--- | :--- | 
| Online | 2020-02-19: Online known account welcome (ABM)
| Online |  2020-02-19: Online catch all 
| Offline | 2020-02-19: Offline catch all

**Live pages**:
* https://about.gitlab.com/pricing/
* https://about.gitlab.com/sales/ 
* https://about.gitlab.com/free-trial/ 
* https://about.gitlab.com/features/ 
* https://about.gitlab.com/comparison/
