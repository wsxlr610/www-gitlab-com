---
layout: handbook-page-toc
title: "TAM and Product Interaction"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

View the [TAM Handbook homepage](/handbook/customer-success/tam/) for additional TAM-related handbook pages.

----

## Feature Requests

If your customer has a feature request, refer to the [example of how to express the customer's interest](https://about.gitlab.com/handbook/product/how-to-engage/#a-customer-expressed-interest-in-a-feature) in an issue.

### Calls with Product

In preparation of a call, make sure to [prepare both the customer and Product](https://about.gitlab.com/handbook/product/how-to-engage/#examples-a-customer-has-a-feature-request) in advance.
