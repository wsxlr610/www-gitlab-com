---
layout: job_family_page
title: "Paralegal"
---

The Paralegal is responsible for supporting the Legal team in areas of commercial contracting, corporate finance, compliance, and other matters as needed. This role reports to the Vice President of Legal - Commercial, IP, and Compliance.

## Responsibilities

* Administer, maintain and manage the legal issue tracker and contract management database systems.
* Prepare standard sales contracts for customers.
* Responding to and running point on customer and vendor questionnaires.
* Assist in-house legal team in coordinating federal, state and local regulatory compliance documents.
* Support corporate governance functions by assisting with drafting reports, maintaining records, completing forms and updating documents.
* Support in-house legal team and outside counsel in preparing documents, conducting due diligence and performing other responsibilities related to corporate acquisitions.
* Conduct research in support of the legal team, as required.
* Work on other projects as assigned.

## Requirements

* Minimum of 5-7 years of experience working in an in-house legal department in a paralegal capacity.
* Paralegal certification preferred but will consider comparable experience in lieu of certification.
* Strong attention to detail and affinity for use of various types of technologies.
* Proactive, dynamic and result driven individual with strong organizational skills.
* Outstanding interpersonal skills, the ability to interface effectively with all business functions throughout the organization.
* Enthusiasm and "self-starter" qualities enabling him or her to manage responsibilities with an appropriate sense of urgency; the ability to function effectively and efficiently in a fast-paced & dynamic environment.
* Superior analytical ability, project management experience, and communication skills
* Ability to manage internal customer priorities and needs.
* A curious mind who does not back down from a challenge.
* Ability to use GitLab

## Performance Indicators
* [Administer, maintain, and manage the legal issue tracker](/handbook/legal/#administer-maintain-and-manage-the-legal-issue-tracker-daily-ongoing--24-hours)
* [Ensure all fully executed vendor contracts are in ContractWorks](/handbook/legal/#ensure-all-fully-executed-vendor-contracts-are-in-contractworks--100)
* [Ensure all fully executed sales contracts are in the Salesforce](/handbook/legal/#ensure-all-fully-executed-sales-contracts-are-in-the-salesforce--100)


## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our team page.

* Selected candidates will be invited to schedule a 45 min [screening call](/handbook/hiring/#screening-call) with our Global Recruiter.
* Next, candidates will be invited to schedule a first interview with the hiring manager.
* Candidates might at this point be invited to schedule with additional team members.
* Successful candidates will subsequently be made an offer via email.
* Previous experience in a Global Start-up and remote first environment would be ideal.
* Successful completion of a background check.

Additional details about our process can be found on our [hiring page](/handbook/hiring).
