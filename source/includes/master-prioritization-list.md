| Priority | Description | Issue label(s) |
| ------ | ------ | ------ |
| 1 | <a href="/handbook/engineering/security/#severity-and-priority-labels-on-security-issues">Security fixes</a> | `security` |
| 2 | Data-loss prevention | `data loss` | 
| 3 | <a href="/handbook/engineering/performance/index.html#availability">Availability</a> | `availability`  | 
| 4 | Fixing regressions (things that worked before) | `regression` |
| 5 | Promised to Customers | `planning-priority`, `customer`, `customer+` |
| 6 | Efficiency Initiatives | |
| 7 | IACV Drivers | |
| 8 | Identified for Dogfooding | `Dogfooding::Build in GitLab`, `Dogfooding::Rebuild in GitLab` |
| 9 | Velocity of new features, user experience improvements, technical debt, community contributions, and all other improvements | `direction`, `feature`, `enhancement`, `technical debt` |
| 10 | Behaviors that yield higher predictability (because this inevitably slows us down) | `predictability` |
